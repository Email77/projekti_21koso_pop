using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_CameraShaker : MonoBehaviour
{
    Mover xMover = null;
    Mover yMover = null;
    Mover zMover = null;
    Vector3 originPos = Vector3.zero;
    const float moveAmount = 0.15f;
    // Start is called before the first frame update
    void Start()
    {
        originPos = transform.localPosition;
        xMover = new Mover();
        yMover = new Mover();
        zMover = new Mover();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 move = new Vector3(xMover.GetMove(), yMover.GetMove() / 2, zMover.GetMove());
        transform.localPosition = originPos + move * moveAmount;
    }

    class Mover
    {
        float speed = 0;
        float offset = 0;

        public Mover()
        {
            speed = Random.Range(0.5f, 2.5f);
            offset = Random.value;
        }

        public float GetMove()
        {
            return Mathf.Sin((Time.time + offset) * speed);
        }
    }
}
