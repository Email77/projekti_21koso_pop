using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_AnimationController : MonoBehaviour
{
    public float speed;
    public Animator _anim;
    float[] currentInput = new float[2];
    void Update()
    {
        if (_anim.GetFloat("leftright") != currentInput[1] || _anim.GetFloat("downup") != currentInput[0])
            AnimateThis(currentInput[1], currentInput[0]);
    }

    public void ReceiveCurrentInput(float[] received)
    {
        if (currentInput != received)
        {
            currentInput[0] = received[0];
            currentInput[1] = received[1];
            Debug.Log(string.Format("{0} {1} {2} {3}", currentInput[0], currentInput[1], received[0], received[1]));
        }
    }

    private void AnimateThis(float leftRight, float downUp)
    {
        float currentLeftRight = _anim.GetFloat("leftright");
        float currentDownUp = _anim.GetFloat("downup");


        if (leftRight < currentLeftRight)
        {
            currentLeftRight -= speed * Time.deltaTime;
            if (currentLeftRight < -1f)
                currentLeftRight = -1f;
        }
        if (leftRight > currentLeftRight)
        {
            currentLeftRight += speed * Time.deltaTime;
            if (currentLeftRight > 1f)
                currentLeftRight = 1f;
        }
        if (downUp < currentDownUp)
        {
            currentDownUp -= speed * Time.deltaTime;
            if (currentDownUp < -1f)
                currentDownUp = -1f;
        }
        if (downUp > currentDownUp)
        {
            currentDownUp += speed * Time.deltaTime;
            if (currentDownUp > 1f)
                currentDownUp = 1f;
        }
        _anim.SetFloat("leftright", currentLeftRight);
        _anim.SetFloat("downup", currentDownUp);
    }
}
