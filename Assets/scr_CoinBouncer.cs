using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_CoinBouncer : MonoBehaviour
{
    Mover yMover = null;
    Vector3 originPos = Vector3.zero;
    const float moveAmount = 0.3f;
    float multiplier;
    // Start is called before the first frame update
    void Start()
    {
        originPos = transform.localPosition;
        yMover = new Mover();
        multiplier = Random.Range(0.8f, 1.2f);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 move = new Vector3(0f, yMover.GetMove(), 0f);
        transform.localPosition = originPos + move * moveAmount;
        transform.Rotate(0f, 90f * multiplier * Time.deltaTime, 0f, Space.Self);
    }

    class Mover
    {
        float speed = 0;
        float offset = 0;

        public Mover()
        {
            speed = Random.Range(0.5f, 2.5f);
            offset = Random.value;
        }

        public float GetMove()
        {
            return Mathf.Sin((Time.time + offset) * speed);
        }
    }
}
