using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_CrashCameraController : MonoBehaviour
{
    private scr_PlayerLocation ref_PlayerLocation;

    void Start()
    {
        ref_PlayerLocation = GetComponent<scr_PlayerLocation>();
    }

    public void CrashCameraActivate()
    {
        ref_PlayerLocation.currentMapSquare.GetComponentInParent<scr_MapSquareFeatures>().crashCameraPosition.gameObject.SetActive(true);
    }
}
