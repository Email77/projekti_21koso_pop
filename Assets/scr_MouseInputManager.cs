﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scr_MouseInputManager : MonoBehaviour
{
    public bool inputReceived;
    public float[] currentInputAxis = new float[2];
    //public float[] lastInputAxis = new float[2]; // tee nyt sama juttu ku itte receivissä ettei se lähetä viittä kerrallaan
    public scr_AnimationController[] animateThese = new scr_AnimationController[2];
    public scr_FlyingController currentControllable;

    public void FinalizeInput()
    {
        for (int i = 0; i < animateThese.Length; i++)
            animateThese[i].ReceiveCurrentInput(currentInputAxis);
        currentControllable.ReceiveCurrentInputState(currentInputAxis);
    }
    public void CheckForInputReceived(bool input)
    {
        if (input)
        {
            inputReceived = true;
            FinalizeInput();    //kaikkia muita tarvitsee, mutta tämä taitaa olla turha, pidetään nyt kuitenkin.
        }
        else
        {
            inputReceived = false;
            currentInputAxis[0] = 0f;
            currentInputAxis[1] = 0f;
            FinalizeInput();
        }
    }

    public void ReceiveInputUpDown(float upDown)
    {
        currentInputAxis[0] = upDown;
        FinalizeInput();
    }
    public void ReceiveInputLeftRight(float leftRight)
    {
        currentInputAxis[1] = leftRight;
        FinalizeInput();
    }
}
