﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_EnviriomentHandler : MonoBehaviour
{
    public scr_MainMenuController ref_MainMenuController;
    public scr_CoinManager coin;

    private static int squareAmountMultiplier = 3;
    public scr_MapSquareFeatures[] inspectorMapSquares = new scr_MapSquareFeatures[squareAmountMultiplier * squareAmountMultiplier];
    private scr_MapSquareFeatures[,] mapSquares = new scr_MapSquareFeatures[squareAmountMultiplier, squareAmountMultiplier];

    void Start()
    {
        CreateHierarchyOnStart();
    }

    private void CreateHierarchyOnStart()
    {
        int counter = 0;
        for (int i = 0; i < squareAmountMultiplier; i++)
        {
            for (int b = 0; b < squareAmountMultiplier; b++)
            {
                mapSquares[i, b] = Instantiate(inspectorMapSquares[counter], new Vector3(i*20, 0, b*20), Quaternion.identity, transform);
                mapSquares[i, b].coordinates[0] = i;
                mapSquares[i, b].coordinates[1] = b;
                counter++;
            }
        }
    }

    public void UpdateRendering(int x, int y)
    {
        for (int i = 0; i < squareAmountMultiplier; i++)
        {
            for (int b = 0; b < squareAmountMultiplier; b++)
            {
                if (x == i - 1 || x == i || x == i + 1)
                {
                    if (y == b - 1 || y == b || y == b + 1)
                    {
                        mapSquares[i, b].EnableRendering();
                    }
                    else
                        mapSquares[i, b].DisableRendering();
                }
                else
                    mapSquares[i, b].DisableRendering();
            }
        }
    }

    public void SendCoinsToCrashCamera(int coinAmount)
    {
        GetComponentInChildren<scr_CoinsCollected>().RefreshCrashCoinsText(coinAmount);
    }
}
