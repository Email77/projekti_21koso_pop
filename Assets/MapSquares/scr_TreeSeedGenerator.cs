using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_TreeSeedGenerator : MonoBehaviour
{
    public scr_MaterialList ref_materialList;
    public Material seed;

    private void Awake()
    {
        seed = ref_materialList.availableMaterials[Random.Range(0, ref_materialList.availableMaterials.Length)];
    }
}
