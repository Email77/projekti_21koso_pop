using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_TreeRandomHeight : MonoBehaviour
{
    public float sizeRange = 1.5f;
    void Start()
    {
        RandomHeight();
        RandomRotation();
    }

    private void RandomHeight()
    {
        float rnd = Random.Range(0f, sizeRange);
        gameObject.transform.position += new Vector3(0f, -rnd, 0f);
    }
    private void RandomRotation()
    {
        //gameObject.transform.Rotate(0f, Random.Range(0f, 360f), 0f);
        gameObject.transform.rotation = Quaternion.Euler(-90f, Random.Range(0f, 360f), 0f);
    }
}
