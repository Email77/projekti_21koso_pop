﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_MapSquareFeatures : MonoBehaviour
{
    public Camera crashCameraPosition;
    public GameObject StartPosition;
    public int[] coordinates = new int[2];
    public GameObject coin;
    public void DisableRendering()
    {
        gameObject.SetActive(false);
    }
    public void EnableRendering()
    {
        gameObject.SetActive(true);
    }

    public Transform ReturnTransform()
    {
        return crashCameraPosition.transform;
    }
    public void ChangeToMenuCamera(int setup)
    {
        GetComponentInParent<scr_EnviriomentHandler>().ref_MainMenuController.MainChangeSetup(setup);
        CrashCameraDisable();
    }
    public void CrashCameraDisable()
    {
        crashCameraPosition.gameObject.SetActive(false);
    }

    private GameObject[] squareCoins = new GameObject[5];
    private void Start()
    {
        coin = GetComponentInParent<scr_EnviriomentHandler>().coin.gameObject;
    }
    private void Update()
    {
        SpawnCoin();
    }
    private void SpawnCoin()
    {
        for (int i = 0; i < squareCoins.Length; i++)
        {
            if (squareCoins[i] == null)
                squareCoins[i] = Instantiate(coin, Vector3.zero, transform.rotation, transform);
        }
    }
}
