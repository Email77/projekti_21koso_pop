using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_RandomMaterialColor : MonoBehaviour
{
    private scr_TreeSeedGenerator ref_seedGenerator; 
    void Start()
    {
        ref_seedGenerator = GetComponentInParent<scr_TreeSeedGenerator>();
        GetComponent<Renderer>().material = ref_seedGenerator.seed;
    }
}
