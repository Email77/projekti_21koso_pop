using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_CoinManager : MonoBehaviour
{
    private void Start()
    {
        Vector3 range = new Vector3(Random.Range(0f, 20f), 100f, Random.Range(0f, 20f));
        transform.localPosition = range;

        if (Physics.Raycast(transform.position, Vector3.down, out RaycastHit hit, 300f))
            transform.localPosition = new Vector3(transform.localPosition.x, hit.point.y + Random.Range(0.7f, 3f), transform.localPosition.z);
    }
}