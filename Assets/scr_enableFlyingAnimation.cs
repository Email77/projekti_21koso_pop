using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_enableFlyingAnimation : MonoBehaviour
{
    private Animator _anim;
    void Start()
    {
        _anim = GetComponent<Animator>();
    }

    public void StartFlyingAnimation()
    {
        _anim.SetFloat("flying", 1f);
    }

    public void StopFlyingAnimation()
    {
        _anim.SetFloat("flying", 0f);
    }
}
