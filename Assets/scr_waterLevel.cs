using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_waterLevel : MonoBehaviour
{
    public bool positionChange;
    Rigidbody rb;
    Vector3 rotation;
    private void Start()
    {
        rb = GetComponentInParent<Rigidbody>();
        rotation = rb.transform.eulerAngles;
    }
    // Update is called once per frame
    void Update()
    {
        if (positionChange)
            transform.position = new Vector3(rb.transform.position.x, -0.5f, rb.transform.position.z);
        transform.eulerAngles = rotation;
    }
}
