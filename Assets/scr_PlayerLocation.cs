using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_PlayerLocation : MonoBehaviour
{
    public GameObject currentMapSquare;

    private bool firstTimeUsed = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "mapSquareTrigger")
        {
            currentMapSquare = other.gameObject;
            int[] currentCoordinates = new int[2];
            currentCoordinates[0] = other.GetComponentInParent<scr_MapSquareFeatures>().coordinates[0];
            currentCoordinates[1] = other.GetComponentInParent<scr_MapSquareFeatures>().coordinates[1];
            other.GetComponentInParent<scr_EnviriomentHandler>().UpdateRendering(currentCoordinates[0], currentCoordinates[1]);

            if (!firstTimeUsed)
            {
                GetComponent<scr_FlyingController>().ApplyStartLocation();
                firstTimeUsed = true;
            }
        }
    }
}
