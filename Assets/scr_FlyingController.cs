﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class scr_FlyingController : MonoBehaviour
{
    public Camera mainCamera;
    public scr_CoinsCollected ref_CoinsCollected;
    public float flySpeed;
    public float riseThisMuchOnStart;
    public bool takeInput = false;

    private Rigidbody rb;
    private ParticleSystem coinParticles;
    private Vector3 startCoordinates;
    private float startFlySpeed;
    private float[] lastState = new float[2];
    private bool startControl = false;
    private bool startTakeOff = false;

    private void Start()
    {
        startFlySpeed = flySpeed;
        rb = gameObject.GetComponent<Rigidbody>();
        coinParticles = GetComponentInChildren<ParticleSystem>();
        lastState[0] = 0f;
        lastState[1] = 0f;
        startCoordinates = rb.transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (takeInput)
        {
            if (startControl)
                Fly(lastState);
            else
            {
                if (lastState[0] != 0f || lastState[1] != 0f)
                    startTakeOff = true;
            }
            if (startTakeOff)
                TakeOff();
        }
    }

    private void TakeOff()
    {
        flySpeed = startFlySpeed;
        rb.transform.Translate(new Vector3(0f, flySpeed * 1.5f * Time.deltaTime, 0f));

        if (rb.transform.position.y >= startCoordinates.y + riseThisMuchOnStart)
        {
            startTakeOff = false;
            startControl = true;
        }
    }

    public void ReceiveCurrentInputState(float[] receivedState)
    {
        if (receivedState != lastState)
        {
            lastState[0] = receivedState[0];
            lastState[1] = receivedState[1];
        }
    }
    private void Fly(float[] receivedState)
    {
        float turningForce;
        float upDownForce;

        turningForce = receivedState[1];
        upDownForce = receivedState[0] * 1.2f;

        rb.transform.Translate(Vector3.forward * flySpeed * Time.deltaTime, Space.Self);
        rb.transform.Translate(new Vector3(0f, upDownForce * Time.deltaTime, 0f));
        rb.transform.Rotate(new Vector3(0f, turningForce * 60 * Time.deltaTime, 0f));
    }

    private void OnCollisionEnter(Collision collision)
    {
        takeInput = false;
        flySpeed = 0;
        startControl = false;
        startTakeOff = true;
        GetComponentInChildren<scr_enableFlyingAnimation>().StopFlyingAnimation();
        ReceiveCurrentInputState(new float[2] { 0f, 0f });
        ApplyCrashCamera();
    }

    private void ApplyCrashCamera()
    {
        GetComponent<scr_CrashCameraController>().CrashCameraActivate();
        mainCamera.gameObject.SetActive(false);
        GetComponentInParent<scr_MainMenuController>().SendInfoToMainMenu(ref_CoinsCollected.coins);
    }

    public void TakeInput()
    {
        takeInput = true;
    }

    public void ApplyStartLocation()
    {
        rb.transform.position = GetComponent<scr_PlayerLocation>().currentMapSquare.GetComponentInParent<scr_MapSquareFeatures>().StartPosition.transform.position;
        rb.transform.rotation = GetComponent<scr_PlayerLocation>().currentMapSquare.GetComponentInParent<scr_MapSquareFeatures>().StartPosition.transform.rotation;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "coin")
        {
            Destroy(other.transform.parent.gameObject);
            coinParticles.Stop();
            coinParticles.Play();
            ref_CoinsCollected.RefreshCoinsText();
        }
    }
}
