using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class scr_CoinsCollected : MonoBehaviour
{

    public TextMeshProUGUI coinsCollected;
    public int coins;

    public void RefreshCoinsText()
    {
        coins++;
        coinsCollected.text = coins.ToString();
    }

    public void RefreshCrashCoinsText(int coinAmount)
    {
        coinsCollected.text = coinAmount.ToString();
    }

    public void ResetCoins()
    {
        coins = 0;
        coinsCollected.text = coins.ToString();
    }

}
