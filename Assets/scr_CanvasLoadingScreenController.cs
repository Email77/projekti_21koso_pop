using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_CanvasLoadingScreenController : MonoBehaviour
{
    public GameObject loadingScreenImage;
    public void ChangeLoadingScreen()
    {
        if (loadingScreenImage.activeSelf == true)
            loadingScreenImage.SetActive(false);
        else
            loadingScreenImage.SetActive(true);
    }
}