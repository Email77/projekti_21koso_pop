using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_MainMenuController : MonoBehaviour
{
    public Setup[] setups = new Setup[2];

    public void MainChangeSetup(int wantedSetup)
    {
        if (setups[wantedSetup].camera.activeSelf == false)
            for (int i = 0; i < setups.Length; i++)
            {
                if (i == wantedSetup)
                    setups[i].ChangeSetup(true);
                else
                    setups[i].ChangeSetup(false);
            }
        GetComponentInChildren<scr_FlyingController>().ApplyStartLocation();
    }

    public scr_EnviriomentHandler ref_EnviriomentHandler;
    public void SendInfoToMainMenu(int coinAmount)
    {
        ref_EnviriomentHandler.SendCoinsToCrashCamera(coinAmount);
    }
}

[System.Serializable]
public class Setup
{
    public GameObject camera;
    public GameObject canvas;

    public void ChangeSetup(bool _bool)
    {
        camera.SetActive(_bool);
        canvas.SetActive(_bool);
    }
}
